package repositoryCatalog;

import domain.*;
import repository.IRepository;

public interface IRepositoryCatalog {

	public IRepository<Order> getOrders();
	public IRepository<OrderItem> getOrderItems();
	public IRepository<ClientDetails> getClientDetails();
	public IRepository<Address> getAddresses();
	public void commit();
}
