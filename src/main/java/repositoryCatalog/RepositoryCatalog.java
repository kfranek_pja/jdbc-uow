package repositoryCatalog;

import java.sql.Connection;

import builder.*;
import domain.*;
import repository.*;
import unitOfWork.IUnitOfWork;

public class RepositoryCatalog implements IRepositoryCatalog{

	private Connection connection;
	private IUnitOfWork uow;
	
	public RepositoryCatalog(Connection connection, IUnitOfWork uow) {
		super();
		this.connection = connection;
		this.uow = uow;
	}


	public IRepository<Order> getOrders() {
		return new OrderRepository(connection, new OrderBuilder(), uow);
	}
	
	public IRepository<OrderItem> getOrderItems() {
		return new OrderItemRepository(connection, new OrderItemBuilder(), uow);
	}
	
	public IRepository<ClientDetails> getClientDetails() {
		return new ClientDetailsRepository(connection, new ClientDetailsBuilder(), uow);
	}
	
	public IRepository<Address> getAddresses() {
		return new AddressRepository(connection, new AddressBuilder(), uow);
	}


	public void commit() {
		uow.commit();
	}

}