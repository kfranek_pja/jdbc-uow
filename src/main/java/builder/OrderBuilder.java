package builder;

import java.sql.ResultSet;
import java.sql.SQLException;

import domain.Order;

public class OrderBuilder implements IdentifiableBuilder<Order>{

	public Order build(ResultSet rs) throws SQLException {
		AddressBuilder addressBuilder = new AddressBuilder();
		ClientDetailsBuilder clientDetailsBuilder = new ClientDetailsBuilder();
		
		Order order = new Order();
		order.setId(rs.getLong("id"));
		order.setDeliveryAddress(addressBuilder.build(rs));
		order.setClient(clientDetailsBuilder.build(rs));
		
		return order;
	}

}
