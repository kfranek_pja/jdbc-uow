package builder;

import java.sql.ResultSet;
import java.sql.SQLException;

import domain.OrderItem;

public class OrderItemBuilder implements IdentifiableBuilder<OrderItem>{

	public OrderItem build(ResultSet rs) throws SQLException {
		OrderItem orderItem = new OrderItem();
		orderItem.setId(rs.getLong("id"));
		orderItem.setName(rs.getString("name"));
		orderItem.setPrice(rs.getDouble("price"));
		orderItem.setDescription(rs.getString("description"));
		return orderItem;
	}

}
