package builder;

import java.sql.ResultSet;
import java.sql.SQLException;

import domain.ClientDetails;

public class ClientDetailsBuilder implements IdentifiableBuilder<ClientDetails>{

	public ClientDetails build(ResultSet rs) throws SQLException {
		ClientDetails clientDetails = new ClientDetails();
		clientDetails.setLogin(rs.getString("login"));
		clientDetails.setName(rs.getString("name"));
		clientDetails.setSurname(rs.getString("surname"));
		return clientDetails;
	}

}
