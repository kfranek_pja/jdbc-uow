package builder;

import java.sql.ResultSet;
import java.sql.SQLException;

import domain.Address;

public class AddressBuilder implements IdentifiableBuilder<Address>{

	public Address build(ResultSet rs) throws SQLException {
		Address address = new Address();
		address.setId(rs.getLong("id"));
		address.setStreet(rs.getString("street"));
		address.setBuildingNumber(rs.getString("building_number"));
		address.setFlatNumber(rs.getString("flat_number"));
		address.setPostalCode(rs.getString("postal_code"));
		address.setCity(rs.getString("city"));
		address.setCountry(rs.getString("country"));
		return address;
	}

}
