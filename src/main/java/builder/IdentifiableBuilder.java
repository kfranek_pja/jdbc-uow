package builder;

import java.sql.ResultSet;
import java.sql.SQLException;

import domain.Identifiable;

public interface IdentifiableBuilder<TIdentifiable extends Identifiable> {

	public TIdentifiable build(ResultSet rs) throws SQLException;
	
}
