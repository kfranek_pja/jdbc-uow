package repository;

import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.List;

import builder.IdentifiableBuilder;
import domain.OrderItem;
import unitOfWork.IUnitOfWork;

public class OrderItemRepository 
	extends Repository<OrderItem>{

	public OrderItemRepository(Connection connection,
			IdentifiableBuilder<OrderItem> builder, IUnitOfWork uow) {
		super(connection, builder, uow);
	}

	@Override
	protected void setUpUpdateQuery(OrderItem orderItem) throws SQLException {
		update.setString(1, orderItem.getName());
		update.setDouble(2, orderItem.getPrice());
		update.setString(3, orderItem.getDescription());
		update.setLong(4, orderItem.getId());
	}

	@Override
	protected void setUpInsertQuery(OrderItem orderItem) throws SQLException {
		insert.setString(1, orderItem.getName());
		insert.setDouble(2, orderItem.getPrice());
		insert.setString(3, orderItem.getDescription());
	}

	@Override
	protected String getTableName() {
		return "order_item";
	}

	@Override
	protected String getUpdateQuery() {
		return "update order_item set (name,price,description)=(?,?,?)"
				+ "where id=?";
	}

	@Override
	protected String getInsertQuery() {
		return "insert into order_item(name,price,description) values(?,?,?)";
	}
	
}