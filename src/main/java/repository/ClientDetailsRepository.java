package repository;

import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.List;

import builder.IdentifiableBuilder;
import domain.ClientDetails;
import unitOfWork.IUnitOfWork;

public class ClientDetailsRepository 
	extends Repository<ClientDetails>{

	public ClientDetailsRepository(Connection connection,
			IdentifiableBuilder<ClientDetails> builder, IUnitOfWork uow) {
		super(connection, builder, uow);
	}

	@Override
	protected void setUpUpdateQuery(ClientDetails clientDetails) throws SQLException {
		update.setString(1, clientDetails.getLogin());
		update.setString(2, clientDetails.getName());
		update.setString(3, clientDetails.getSurname());
		update.setLong(4, clientDetails.getId());
	}

	@Override
	protected void setUpInsertQuery(ClientDetails clientDetails) throws SQLException {
		insert.setString(1, clientDetails.getLogin());
		insert.setString(2, clientDetails.getName());
		insert.setString(3, clientDetails.getSurname());
	}

	@Override
	protected String getTableName() {
		return "client_details";
	}

	@Override
	protected String getUpdateQuery() {
		return "update client_details set (login,name,surname)=(?,?,?)"
				+ "where id=?";
	}

	@Override
	protected String getInsertQuery() {
		return "insert into client_details(login,name,surname) values(?,?,?)";
	}
	
}