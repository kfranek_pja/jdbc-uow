package repository;

import java.util.List;

public interface IRepository<TIdentifiable> {

	public void save(TIdentifiable idf);
	public void update(TIdentifiable idf);
	public void delete(TIdentifiable idf);
	public TIdentifiable get(long id);
	public List<TIdentifiable> getAll();
}