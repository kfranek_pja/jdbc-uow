package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import builder.IdentifiableBuilder;
import unitOfWork.IUnitOfWork;
import unitOfWork.IUnitOfWorkRepository;
import domain.Identifiable;

public abstract class Repository<TIdentifiable extends Identifiable> 
	implements IRepository<TIdentifiable>, IUnitOfWorkRepository
{
	
	protected IUnitOfWork uow;
	protected Connection connection;
	protected PreparedStatement selectByID;
	protected PreparedStatement insert;
	protected PreparedStatement delete;
	protected PreparedStatement update;
	protected PreparedStatement selectAll;
	protected IdentifiableBuilder<TIdentifiable> builder;

	protected String selectByIDSql=
			"SELECT * FROM "
			+ getTableName()
			+ " WHERE id=?";
	protected String deleteSql=
			"DELETE FROM "
			+ getTableName()
			+ " WHERE id=?";
	protected String selectAllSql=
			"SELECT * FROM " + getTableName();
	
	protected Repository(Connection connection,
			IdentifiableBuilder<TIdentifiable> builder, IUnitOfWork uow)
	{
		this.uow=uow;
		this.builder=builder;
		this.connection = connection;
		try {
			selectByID=connection.prepareStatement(selectByIDSql);
			insert = connection.prepareStatement(getInsertQuery());
			delete = connection.prepareStatement(deleteSql);
			update = connection.prepareStatement(getUpdateQuery());
			selectAll = connection.prepareStatement(selectAllSql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	
	public void save(TIdentifiable idf)
	{
		uow.markAsNew(idf, this);
	}

	
	public void update(TIdentifiable idf)
	{
		uow.markAsDirty(idf, this);
	}

	
	public void delete(TIdentifiable idf)
	{
		uow.markAsDeleted(idf, this);
	}

	
	@SuppressWarnings("unchecked")
	public void persistAdd(Identifiable idf) {

		try {
			setUpInsertQuery((TIdentifiable)idf);
			insert.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


	
	@SuppressWarnings("unchecked")
	public void persistUpdate(Identifiable idf) {

		try {
			setUpUpdateQuery((TIdentifiable)idf);
			update.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	public void persistDelete(Identifiable idf) {

		try {
			delete.setLong(1, idf.getId());
			delete.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	
	public TIdentifiable get(long id) {

		try {
			selectByID.setLong(1, id);
			ResultSet rs = selectByID.executeQuery();
			while(rs.next())
			{
				return builder.build(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}



	
	public List<TIdentifiable> getAll() {

		List<TIdentifiable> result = new ArrayList<TIdentifiable>();
		
		try {
			ResultSet rs= selectAll.executeQuery();
			while(rs.next())
			{
				result.add(builder.build(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}

	
	protected abstract void setUpUpdateQuery(TIdentifiable entity) throws SQLException;
	protected abstract void setUpInsertQuery(TIdentifiable entity) throws SQLException;
	protected abstract String getTableName();
	protected abstract String getUpdateQuery();
	protected abstract String getInsertQuery();
}
