package repository;

import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.List;

import builder.IdentifiableBuilder;
import domain.Order;
import unitOfWork.IUnitOfWork;

public class OrderRepository 
	extends Repository<Order>{

	public OrderRepository(Connection connection,
			IdentifiableBuilder<Order> builder, IUnitOfWork uow) {
		super(connection, builder, uow);
	}

	@Override
	protected void setUpUpdateQuery(Order order) throws SQLException {
		update.setLong(1, order.getClient().getId());
		update.setLong(2, order.getDeliveryAddress().getId());
		update.setLong(3, order.getId());
	}

	@Override
	protected void setUpInsertQuery(Order order) throws SQLException {
		insert.setLong(1, order.getClient().getId());
		insert.setLong(2, order.getDeliveryAddress().getId());
	}

	@Override
	protected String getTableName() {
		return "order_";
	}

	@Override
	protected String getUpdateQuery() {
		return "update order_ set (client,delivery_address)=(?,?)"
				+ "where id=?";
	}

	@Override
	protected String getInsertQuery() {
		return "insert into order_(client,delivery_address) values(?,?)";
	}
	
}