package repository;

import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.List;

import builder.IdentifiableBuilder;
import domain.Address;
import unitOfWork.IUnitOfWork;

public class AddressRepository 
	extends Repository<Address>{

	public AddressRepository(Connection connection,
			IdentifiableBuilder<Address> builder, IUnitOfWork uow) {
		super(connection, builder, uow);
	}

	@Override
	protected void setUpUpdateQuery(Address address) throws SQLException {
		//(street,building_number,flat_number,postal_code,city,country)
		update.setString(1, address.getStreet());
		update.setString(2, address.getBuildingNumber());
		update.setString(3, address.getFlatNumber());
		update.setString(4, address.getPostalCode());
		update.setString(5, address.getCity());
		update.setString(6, address.getCountry());
		update.setLong(7, address.getId());
	}

	@Override
	protected void setUpInsertQuery(Address address) throws SQLException {
		insert.setString(1, address.getStreet());
		insert.setString(2, address.getBuildingNumber());
		insert.setString(3, address.getFlatNumber());
		insert.setString(4, address.getPostalCode());
		insert.setString(5, address.getCity());
		insert.setString(6, address.getCountry());
	}

	@Override
	protected String getTableName() {
		return "address";
	}

	@Override
	protected String getUpdateQuery() {
		return "update address set (street,building_number,flat_number,postal_code,city,country)=(?,?,?,?,?,?)"
				+ "where id=?";
	}

	@Override
	protected String getInsertQuery() {
		return "insert into address(street,building_number,flat_number,postal_code,city,country) values(?,?,?,?,?,?)";
	}
	
}