package unitOfWork;

import domain.Identifiable;

public interface IUnitOfWork {

	public void commit();
	public void rollback();
	public void markAsNew(Identifiable idf, IUnitOfWorkRepository repository);
	public void markAsDirty(Identifiable idf, IUnitOfWorkRepository repository);
	public void markAsDeleted(Identifiable idf, IUnitOfWorkRepository repository);
}
