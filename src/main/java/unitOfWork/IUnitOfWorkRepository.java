package unitOfWork;

import domain.Identifiable;

public interface IUnitOfWorkRepository {

	public void persistAdd(Identifiable idf);
	public void persistUpdate(Identifiable idf);
	public void persistDelete(Identifiable idf);
}
