package unitOfWork;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import domain.Identifiable;
import domain.IdentifiableState;

public class UnitOfWork implements IUnitOfWork{

	
	private Connection connection;
	
	private Map<Identifiable, IUnitOfWorkRepository> idfs = 
			new LinkedHashMap<Identifiable, IUnitOfWorkRepository>();
	
	public UnitOfWork(Connection connection) {
		super();
		this.connection = connection;
		
		try {
			connection.setAutoCommit(false);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void commit() {

		for(Identifiable idf: idfs.keySet())
		{
			switch(idf.getState())
			{
			case Changed:
				idfs.get(idf).persistUpdate(idf);
				break;
			case Deleted:
				idfs.get(idf).persistDelete(idf);
				break;
			case New:
				idfs.get(idf).persistAdd(idf);
				break;
			case Unchanged:
				break;
			default:
				break;}
		}
		
		try {
			connection.commit();
			idfs.clear();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public void rollback() {

		idfs.clear();
		
	}

	public void markAsNew(Identifiable idf, IUnitOfWorkRepository repository) {
		idf.setState(IdentifiableState.New);
		idfs.put(idf, repository);
		
	}

	public void markAsDirty(Identifiable idf, IUnitOfWorkRepository repository) {
		idf.setState(IdentifiableState.Changed);
		idfs.put(idf, repository);
		
	}

	public void markAsDeleted(Identifiable idf, IUnitOfWorkRepository repository) {
		idf.setState(IdentifiableState.Deleted);
		idfs.put(idf, repository);
		
	}

}
