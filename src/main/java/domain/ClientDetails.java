package domain;

//Szczegoly danego klienta

public class ClientDetails extends Identifiable  {
	
	private String login;
	private String name;
	private String surname;
	
	public ClientDetails() {
		super();
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSurname() {
		return this.surname;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public String getLogin() {
		return this.login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
}