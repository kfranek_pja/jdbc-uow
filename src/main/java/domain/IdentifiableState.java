package domain;

public enum IdentifiableState {
	New, Changed, Unchanged, Deleted
}