package domain;

//Klasa pozycji na zamowieniu

public class OrderItem extends Identifiable  {
	
	private String name;
	private String description;
	private double price;
	
	public OrderItem() {
		super();
	}
	
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public double getPrice() {
		return this.price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
}