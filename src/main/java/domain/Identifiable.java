package domain;

public abstract class Identifiable {

	private long id;

	IdentifiableState state;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public IdentifiableState getState() {
		return state;
	}

	public void setState(IdentifiableState state) {
		this.state = state;
	}
	
	
}