package domain;

import java.util.ArrayList;
import java.util.List;

//Klasa zamowienia

public class Order extends Identifiable  {
	
	private ClientDetails client;
	private List<OrderItem> items;
	private Address deliveryAddress;
	
	public Order() {
		super();
		this.items = new ArrayList<OrderItem>();
	}
	
	public ClientDetails getClient() {
		return this.client;
	}
	public void setClient(ClientDetails client) {
		this.client = client;
	}
	
	public Address getDeliveryAddress() {
		return this.deliveryAddress;
	}
	
	public void setDeliveryAddress(Address deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
	
	public List<OrderItem> getItems() {
		return this.items;
	}
	
	public void setItems(List<OrderItem> items) {
		this.items = items;
	}
	
}